import { Component, ElementRef, Input, OnInit, ViewChild, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { timeout } from '../../../../../node_modules/@types/q';
import { container } from '../../../../../node_modules/@angular/core/src/render3/instructions';

declare var jQuery: any;
declare var d3: any;
declare var DonutCharts: any;
declare var $: any;
var pt: any;
@Component({
  selector: 'pieChart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit, OnChanges  {

  @ViewChild('pieChart') pieChart: ElementRef;
  @Input() data: Object;
  @Output() onChangedSelection: EventEmitter<object> = new EventEmitter<object>();

  donuts: any;
  container: any;

  constructor() { }
  
  ngOnInit() {   
  }

  ngOnChanges(changes: SimpleChanges) {
    pt = this;
    if (changes.data.firstChange) {
      this.container = document.getElementById('pie_chart')
      this.container.innerHTML='';
      this.donuts = new DonutCharts('#pie_chart');
      this.donuts.create(this.data);     
    } else {
      this.container.removeEventListener("onselect", this.onSelected);
      this.donuts.update(this.data);
    }
    this.container.addEventListener("onselect", this.onSelected);

  }

  onSelected(e) {
    pt.onChangedSelection.emit(e.detail.data);
  }
}
