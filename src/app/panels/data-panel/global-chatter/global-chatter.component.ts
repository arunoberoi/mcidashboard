import {
	Component,
	OnInit,
	ViewChild,
	Input,
	EventEmitter,
	Output
} from '@angular/core';

import {
	AppService
} from '../../../app.service';

import {
	Constants
} from '../../../common/constants'

import {
	DataTable
} from '../../../../../node_modules/angular-6-datatable';

@Component({
  selector: 'app-global-chatter',
  templateUrl: './global-chatter.component.html',
  styleUrls: ['./global-chatter.component.scss']
})
export class GlobalChatterComponent implements OnInit {

  data: any;
  currentPage: number;
  totalPages: number;
  totalRows: number;
  rowsOnPage: number;

  dataBrantoStats: any;
  dataTop3Voices: any;
  dataTop3Channels: any;

  isArticleView: boolean = false;
  /* global filters */
	filter: any;

  @ViewChild('mf') childDataTable:DataTable;

  constructor(public appService: AppService) {
		this.appService.onCurrentSectionChanged.subscribe(section => {
			this.getData();
    });
    
		this.appService.onCurrentDashboardChanged.subscribe(dashboard => {
			this.getData();
		});
  
  }

	getData() {
		if (this.appService.currentSection.articles && this.appService.currentSection.articles.length) {
      this.isArticleView = true;
		} else {
			this.isArticleView = false; // global view 
			let today = new Date();
			let ymd = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;

			let currentFilter = {
				clusters: this.appService.currentSection.clusters,
				platforms: this.appService.currentSection.platforms,
				startDate: this.appService.currentDashboard.period.startDate,
				endDate: this.appService.currentDashboard.period.endDate
			}
			/* if not setted dates, use today */
			if (!currentFilter.startDate) {
				currentFilter.startDate = ymd;
				currentFilter.endDate = ymd;
			}
			if (JSON.stringify(currentFilter) != JSON.stringify(this.filter)) {
        this.filter = { ...currentFilter};

        /* chatter stats (articles and addtional values */
				this.appService.getChatterStats(currentFilter).subscribe(data => {
					this.data = data;
					this.totalRows = this.data.length;
					this.totalPages = Math.ceil(this.totalRows / this.rowsOnPage);
					this.currentPage = 1
        })
        
        /* branto stats 3(4) values */
        this.appService.getBrantoStats(currentFilter).subscribe(data => {
          this.dataBrantoStats = data;
        })

        /* branto top 3 voices list */
        this.appService.getBrantoTopVoices(currentFilter).subscribe(data => {
          this.dataTop3Voices = data;
        })

        /* branto top 3 channels list */
        this.appService.getBrantoTopChannels(currentFilter).subscribe(data => {
          this.dataTop3Channels = data;
        })


			}

		}
	}

  
  ngOnInit() {
    this.rowsOnPage = 4;
  }

  setPage (pageNumber) {
    this.currentPage = pageNumber
    this.childDataTable.setPage(this.currentPage, this.rowsOnPage)
  }
  hideAllMenu () {
    let menuList =  Array.from(document.querySelectorAll('div.menu-wrapper') as HTMLCollectionOf<HTMLElement>);
    menuList.forEach(function(element) {
      element.style.display = 'none';
    });
  }

}
