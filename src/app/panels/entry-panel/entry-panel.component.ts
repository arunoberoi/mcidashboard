import { Component, OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';
import { AppService } from '../../app.service';
import { Constants } from '../../common/constants';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-entry-panel',
  templateUrl: './entry-panel.component.html',
  styleUrls: ['./entry-panel.component.scss']
})



export class EntryPanelComponent implements OnInit, OnDestroy {
  @Input() entryList: any;
  @Output() onClickHide: EventEmitter<object> = new EventEmitter<object>();
  @Output() onSelectEntry: EventEmitter<object> = new EventEmitter<object>();

  /* global service variable */
  currentDashboard: any;
  isSaved: boolean;
  onCurrentDashboardChangedSubscription: Subscription;

  constructor(public appService: AppService) {
    this.onCurrentDashboardChangedSubscription = appService.onCurrentDashboardChanged.subscribe(dashboard => {
      this.currentDashboard = dashboard;
    });
  }

  ngOnDestroy() {
    this.onCurrentDashboardChangedSubscription.unsubscribe;
  }

  ngOnInit() {
  }

  setDefaultEntry() {
    this.appService.setCurrentDashboard(Constants.DEFAULT_DASHBOARD);
    this.appService.setSaveState (false);
  }

  selectEntry (index) {
    this.appService.setCurrentDashboard({...this.entryList[index]});
    this.appService.setSaveState (true);
    this.onSelectEntry.emit();
  }

  hideSidbar() {
    this.onClickHide.emit();
  }

  onClickLogo() {
    this.appService.setCurrentSectionChanged({});
  }
}
