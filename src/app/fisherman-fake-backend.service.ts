import { InMemoryDbService } from 'angular-in-memory-web-api';

import { FishermanDB } from './common/fakeData';

export class FishermanFakeBackendService implements InMemoryDbService {
  createDb() {
    return {
      'entry_list': FishermanDB.entryList,
      'clusters': FishermanDB.clusterData,
      'platforms': FishermanDB.platformData,
      'articles': FishermanDB.articleData,
      'consumption': FishermanDB.consumptionData,
      'branto_stats': FishermanDB.brandtoStats,
      'chatter_stats': FishermanDB.chatterStats,
      'branto_top_voices': FishermanDB.brandtoTopVoices,
      'branto_top_channels': FishermanDB.brandtoTopChannels
    };
  }
}
