import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from './common/constants'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import "rxjs/add/operator/map";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AppService {

  /* state variables */

  isSaved: boolean = false;
  onIsSavedChanged: BehaviorSubject<any> = new BehaviorSubject(this.isSaved);

  currentDashboard: any = Constants.DEFAULT_DASHBOARD;
  onCurrentDashboardChanged: BehaviorSubject<any> = new BehaviorSubject(this.currentDashboard);

  /* for data section */
  currentSection: any;
  onCurrentSectionChanged: BehaviorSubject<any> = new BehaviorSubject({});


  baseURL = Constants.URL.baseURL;


  constructor(private http : HttpClient) {     
  }

  setCurrentDashboard(obj) {
    this.currentDashboard = {...obj};       
    this.onCurrentDashboardChanged.next(this.currentDashboard);
  }

  setSaveState(val) {
    this.isSaved = val;
    this.onIsSavedChanged.next(val);
  }

  setCurrentSectionChanged (obj) {
    this.currentSection = {...obj};
    this.onCurrentSectionChanged.next(this.currentSection)
  }
 

//////////////////////////////////////////////////////////////////////////////////////
///////// HOOK API ///////////
////////////////////

  /* get saved list */
  getAllEntryList() {
    return this.http
      .get (Constants.URL.baseURL + '/entry_list')
  }
  /* updates a entry */
  updateEntry(entry) {
    return this.http
      .put (Constants.URL.baseURL + '/entry_list', entry)
  }
  /* create new entry (dashboard) */ 
  createEntry(entry) {
    return this.http
      .post (Constants.URL.baseURL + '/entry_list', entry)
  }

  /* get clusters */
  getClusters (filter) {
    return this.http
      .get (Constants.URL.baseURL + '/clusters', filter)
  }

  /* get platform */
  getPlatforms (filter) {
    return this.http
      .get (Constants.URL.baseURL + '/platforms', filter)
  }

  /* get articles */
  getArticles (filter) {
    return this.http
      .get (Constants.URL.baseURL + '/articles', filter)
  }

  /* get consumption */
  getConsumption (filter) {
    return this.http
      .get (Constants.URL.baseURL + '/consumption', filter)
  }

  /* get branto_states */
  getBrantoStats (filter) {
    return this.http
      .get (Constants.URL.baseURL + '/branto_stats', filter)
  }

  /* get chatter_states */
  getChatterStats (filter) {
    return this.http
      .get (Constants.URL.baseURL + '/chatter_stats', filter)
  }

  /* get branto_top_voices */
  getBrantoTopVoices (filter) {
    return this.http
      .get (Constants.URL.baseURL + '/branto_top_voices', filter)
  }

  /* get branto_top_channels */
  getBrantoTopChannels (filter) {
    return this.http
      .get (Constants.URL.baseURL + '/branto_top_channels', filter)
  }


}
 