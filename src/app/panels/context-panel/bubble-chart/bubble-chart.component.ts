import { Component, ElementRef, Input, OnInit, ViewChild, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { AppService } from '../../../app.service';

declare var jQuery: any;
declare var D3Bubbles: any;
declare var d3: any;
declare var $: any;
var pt: any;

@Component({
  selector: 'app-bubble-chart',
  templateUrl: './bubble-chart.component.html',
  styleUrls: ['./bubble-chart.component.css']
})

export class BubbleChartComponent implements OnInit, OnChanges {
  
  @Input() chartWidth: number;
  @Input() chartHeight: number;
  @Input() data: Object;
  
  @Output() onClickBubble: EventEmitter<object> = new EventEmitter<object>();

  @ViewChild('wrapper') wrapper: ElementRef;

  chartId: string;
  container: any;
  
  currentCluster = null;

  constructor(public appService: AppService) { 
    this.appService.onCurrentSectionChanged.subscribe(section => { 
      if (!section.clusters || !section.clusters.length) {
        this.currentCluster = null;
      } else {
        this.currentCluster = section.clusters[0];
      }
      d3.selectAll('.bubble-outline').attr('stroke', 'transparent');
      d3.select('.outline-' + this.currentCluster).attr('stroke', 'black');
    });
  }

  ngOnInit() {  
    
    this.chartId = Math.floor(Date.now() * Math.random()).toString(36);
  }

  ngOnChanges(changes: SimpleChanges) {
    pt = this;
    this.drawChart();
  }

  drawChart() {
    

    if (this.container) {
      this.container.removeEventListener('dragend', this.selectedBubble);
    } else {
      this.container = document.querySelector(`#wrapper_clusters`);
    }
    
    
    let bubbles = new D3Bubbles({
      wrapper: `#wrapper_clusters`,
      container: `#wrapper_clusters .plot`,
      bubbles: {
        bubbles_per_px: 0.02,
        bubbles_max: 'auto'
      },
      width: this.chartWidth,
      height: this.chartHeight,
      colors: {
        bubbles:['#FFC400', '#DD2C00'],
        texts:['#FFF'],
      },
      radius: {
        min: 20,
        max: 80
      },
      features: {
        dragmove: true,
        overflow: false,
        fit_texts: false
      },
      data: this.data
    });
    

    bubbles.init();
    bubbles.visualize();

    
    this.container.addEventListener('dragend', this.selectedBubble );

  }

  selectedBubble(e) {
    pt.onClickBubble.emit(e.detail.data);
 }
}

