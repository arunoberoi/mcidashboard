import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from '../../../../../node_modules/angular-6-datatable';
import { AppService } from '../../../app.service';
import { Constants } from '../../../common/constants'


@Component({
  selector: 'app-global-engagement',
  templateUrl: './global-engagement.component.html',
  styleUrls: ['./global-engagement.component.scss']
})
export class GlobalEngagementComponent implements OnInit {

  data0: any[];
  currentPage0: number;
  totalPages0: number;
  totalRows0: number;
  rowsOnPage0: number;

  currentArticle: any;

  data1: any[];
  currentPage1: number;
  totalPages1: number;
  totalRows1: number;
  rowsOnPage1: number;

  collapsed0: number;

  @ViewChild('mf0') childDataTable0:DataTable;
  @ViewChild('mf1') childDataTable1:DataTable;
	constructor(public appService: AppService) {

		this.appService.onCurrentSectionChanged.subscribe(section => {
			this.getArticlesData();
    });
    
		this.appService.onCurrentDashboardChanged.subscribe(dashboard => {
			this.getArticlesData();
		});

  }
  
  getArticlesData() {
		if (!this.appService.currentSection.articles || !this.appService.currentSection.articles.length) {
      this.currentArticle = false;
    } else {
      this.currentArticle = true;
    }
  }
  ngOnInit() {
    this.rowsOnPage0 = 5;

    this.data0 = [
      {article: 'year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91},
      {article: 'year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91},
      {article: 'year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91},
      {article: 'year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91},
      {article: 'year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91},
      {article: 'year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91},
      {article: 'year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91},
      {article: 'year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91},
      {article: 'year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91},
      {article: 'year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91},
      {article: 'year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91}
    ];

    this.totalRows0 = this.data0.length;
    this.totalPages0 = Math.ceil(this.totalRows0 / this.rowsOnPage0);
    this.currentPage0 = 1;


    this.rowsOnPage1 = 5;

    this.data1 = [
      {article: '1year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'10 Jan 2018, 1715hrs | Straits Times', p1: 14, p2: 73, p3: 14 , p4:91, p5:92},
      {article: '2year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 14, p2: 73, p3: 14 , p4:91, p5:92},
      {article: '3year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'10 Jan 2018, 1715hrs | Straits Times', p1: 14, p2: 73, p3: 14 , p4:91, p5:92},
      {article: '4year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 23, p2: 73, p3: 14 , p4:91, p5:92},
      {article: '5year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'10 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91, p5:92},
      {article: '6year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91, p5:92},
      {article: '7year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'10 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91, p5:92},
      {article: '8year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91, p5:92},
      {article: '9year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91, p5:92},
      {article: '0year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91, p5:92},
      {article: '3year-new-old-chinese-singapore-time-last-dog-man-people...boy-inc-home-student', dateTime:'20 Jan 2018, 1715hrs | Straits Times', p1: 24, p2: 73, p3: 14 , p4:91, p5:92}
    ];

    this.totalRows1 = this.data0.length;
    this.totalPages1 = Math.ceil(this.totalRows1 / this.rowsOnPage1);
    this.currentPage1 = 1;
    this.collapsed0 = -1;
  }

  setPage0 (pageNumber) {
    this.currentPage0 = pageNumber
    this.childDataTable0.setPage(this.currentPage0, this.rowsOnPage0)
    this.collapsed0 = -1;
  }

  setPage1 (pageNumber) {
    this.currentPage1 = pageNumber
    this.childDataTable1.setPage(this.currentPage1, this.rowsOnPage1)
  }

  hideAllMenu () {
    let menuList =  Array.from(document.querySelectorAll('div.menu-wrapper') as HTMLCollectionOf<HTMLElement>);
    menuList.forEach(function(element) {
      element.style.display = 'none';
    });
  }
}
