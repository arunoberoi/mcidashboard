import { Component, OnInit } from '@angular/core';
import { Constants } from '../../common/constants';
import { ScrollToAnimationEasing, ScrollToEvent, ScrollToOffsetMap } from '@nicky-lenaers/ngx-scroll-to';
import { AppService } from '../../app.service';

var pt;
@Component({
  selector: 'app-data-panel',
  templateUrl: './data-panel.component.html',
  styleUrls: ['./data-panel.component.scss']
})
export class DataPanelComponent implements OnInit {

  currentView: number = Constants.VIEW_GLOBAL;
  constants: object = Constants;
  clusters: any;
  platforms: any;
  articles: any;

  scrollDuration: number = 1000;
  scrollEasing: ScrollToAnimationEasing = 'easeInOutCubic';
  currentTab: number = 0;

  showTabMenu: boolean = false;
  viewCluster: number = Constants.VIEW_CLUSTER

  constructor(public appService: AppService) { 
    this.appService.onCurrentSectionChanged.subscribe(section => {   
      this.clusters = section.clusters;
      this.platforms = section.platforms;
      this.articles = section.articles;
      if (this.articles && this.articles.length) {
        this.currentView = Constants.VIEW_ARTICLE;
      } else if (this.clusters && this.clusters.length) {
        this.currentView = Constants.VIEW_CLUSTER;
      } else {
        this.currentView = Constants.VIEW_GLOBAL;
      }
    });
  }
  
  ngOnInit() {
    pt = this;
    window.addEventListener('scroll', this.onScroll, true);
  }
  ngOnDestroy() {
    window.removeEventListener('scroll', this.onScroll, true);
  }

  onScroll(e){
    if (e.target == document.getElementsByClassName('content-panel')[0]) {
      if (e.target.scrollTop + 100 < document.getElementById('consumption').offsetTop  ) {
        pt.currentTab = 0;
      } else if (e.target.scrollTop + 100 < document.getElementById('chatter').offsetTop ) {
        pt.currentTab = 1;
      } else if (e.target.scrollTop + 100 < document.getElementById('engagement').offsetTop ) {
        pt.currentTab = 2;
      } else {
        pt.currentTab = 3;
      }
    }
  }

  
}
