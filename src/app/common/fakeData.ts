export class FishermanDB {
  public static entryList = [
    {
      id: '0K3DlfksiLFDksdlK2',
      title: '2018/07/28', 
      search: {term: '', part: '0'}, 
      period: {index:0, startDate: '2018-07-09', endDate: '2018-07-14'},
      clusters: {items: 10, sort: [], page: 1},
      platforms: {items: 10, page: 1},
      articles: {page: 1},
      updated: '2018-07-30'
    },
    {
      id: '0KDlfksiLFDks8lK2',
      title: '2018/09/21', 
      search: {term: 'aaa', part: '1'}, 
      period: {index:1, startDate: '2018-07-09', endDate: '2018-07-14'},
      clusters: {items: 10, sort: [], page: 1},
      platforms: {items: 10, page: 1},
      articles: {page: 1},
      updated: '2018-07-31'
    },
    {
      id: '0KDlfksiLFDkddl1K',
      title: '2018/10/22', 
      search: {term: '', part: '2'}, 
      period: {index:2, startDate: '2018-07-09', endDate: '2018-07-14'},
      clusters: {items: 10, sort: [], page: 1},
      platforms: {items: 10, page: 1},
      articles: {page: 1},
      updated: '2018-07-21'
    }
  ];

  public static clusterData = [
    {id: '01', name: 'budget <br> tax <br> neighbor', radius: 50, label: '100'},
    {id: '02', name: 'punggol - old <br> man mrt', radius: 10, label: '27'},
    {id: '03', name: 'diversity', radius: 5, label: '5'},
    {id: '04', name: 'water pump <br> replacement', radius: 30, label: '30'},
    {id: '05', name: 'mccy <br> grace fu', radius: 17, label: '17'},
    {id: '06', name: 'shorter - train <br> discrimination', radius: 50, label: '100'},
    {id: '07', name: 'nixx aaaa bbb', radius: 80, label: '80'},
    {id: '08', name: 'dsfdsf adfs <br>asasdf asff <br>sfas asfasfsadf', radius: 90, label: '100'},
    {id: '09', name: 'cccc cccc - ccccccc', radius: 100, label: '100'},
    {id: '10', name: 'abababds <br>dfsaf', radius: 80, label: '80'}
  ];

  public static platformData = [
    {id:'01',cat:'Straits Times', val: 10},
    {id:'02',cat:'The Smart Local', val: 50},
    {id:'03',cat:'Channel New Asia', val: 40},
    {id:'04',cat:'Yahoo Singapore', val: 40},
    {id:'05',cat:'Mothersip', val: 40},
    {id:'06',cat:'Today Online', val: 40},
    {id:'07',cat:'The Independent', val: 40},
    {id:'08',cat:'Others', val: 40},
    {id:'09',cat:'States', val: 40},
    {id:'10',cat:'Today Online', val: 40},
  ];

  public static articleData = [
    { 
      id: '01', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40]
    },
    { 
      id: '02', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40]
    },
    { 
      id: '03', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40]
    },
    { 
      id: '04', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40]
    },
    { 
      id: '05', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40]
    },
    { 
      id: '06', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40]
    },
    { 
      id: '07', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40]
    },
    { 
      id: '08', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40]
    },
    { 
      id: '09', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40]
    }
  ]

  public static consumptionData = [
    { 
      id: '01', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40],
      consumption: 85
    },
    { 
      id: '02', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40],
      consumption: 85
    },
    { 
      id: '03', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40],
      consumption: 85    
    },
    { 
      id: '04', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40],
      consumption: 85    
    },
    { 
      id: '05', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40],
      consumption: 85    
    },
    { 
      id: '06', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40],
      consumption: 85    
    },
    { 
      id: '07', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40],
      consumption: 85    
    },
    { 
      id: '08', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40],
      consumption: 85    
    },
    { 
      id: '09', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40],
      consumption: 85
    }
  ];

  public static brandtoStats = {
    total_records: 50,
    articles: 50,
    comments: 30,
    voice: 30
  };

  public static chatterStats = [
    { 
      id: '01', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40],
      stats: [20, 30]
    },
    { 
      id: '02', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40],
      stats: [20, 30]
    },
    { 
      id: '03', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40],
      stats: [20, 30]
    },
    { 
      id: '04', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40],
      stats: [20, 30]
    },
    { 
      id: '05', 
      title: 'Grace Fu thins small social circles are natural and more...', 
      date: '20 Jan 2018, 1715hrs', 
      place:'Today Online', 
      type: '0', 
      score: [30, 40, 20, 23, 20, 42, 50, 20, 40],
      stats: [20, 30]
    }

  ];

  public static brandtoTopVoices = [
    {
      name: 'Karen koh',
      value: 99
    },
    {
      name: 'Karen koh',
      value: 99
    },
    {
      name: 'Karen koh',
      value: 99
    }
  ];

  public static brandtoTopChannels = [
    {
      name: 'facebook xxxx',
      value: 99
    },
    {
      name: 'facebook xxxx1',
      value: 99
    },
    {
      name: 'facebook xxxx2',
      value: 99
    }
  ]


}