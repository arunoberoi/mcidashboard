import { Component, ElementRef, OnInit, HostListener, SimpleChanges, OnChanges, Input, ViewChild, Output, EventEmitter, SimpleChange, OnDestroy } from '@angular/core';
import { AppService } from './app.service';
import { Constants } from './common/constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{

  title = 'Lazy loading feature modules';
  entrySidebar: boolean;
  contextSidebar: number;

  entryList: any;
  screenWidth: number = 500;
  currentTab: any;
  @ViewChild('mainContainer') mainContainer: ElementRef;


  constructor(public appService: AppService) {}
  ngAfterContentInit(){
  // ngAfterViewInit(){
    this.screenWidth = this.mainContainer.nativeElement.clientWidth;
    this.updateSidbar();
  }  
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth = event.target.innerWidth;
    this.updateSidbar();
  }

  updateSidbar() {
    setTimeout (()=>{
      if (this.screenWidth > 1328) {
        this.entrySidebar = false;
        this.contextSidebar = 1;
      } else {
        this.entrySidebar = false;
        this.contextSidebar = 1;
      }
    },0);
  }

  ngOnInit() {
    this.entrySidebar = true;
    this.contextSidebar = 1;
    this.getEntryList();
  }
  ngOnDestroy() {
  }

  onScroll(e){
  }


  onUpdateEntry(entry) {
    let isNew = true;
    for (let i = 0; i < this.entryList.length; i ++) {
      if (this.entryList[i].id === entry.id) {
        this.entryList[i] = {...entry};
        isNew = false;
      }
    }
    if (isNew) {
      this.entryList.push(entry);
    }
  }


  getEntryList () {
    try {
      this.appService.getAllEntryList()
        .subscribe(data => {
          this.entryList = data;
        });
    } catch (e) {
      console.log(e);
    }
  }

  getContextWidth () {
    let contextWidth = this.screenWidth/3; // 30% normal
    if (contextWidth < 230) { // 100% for small device
      contextWidth = this.screenWidth;
    } else if ( contextWidth < 500) { // medium 30% width is less than 500px
      contextWidth = 500;
    }    
    return contextWidth;
  }

  getContextStyle () {
    return {'margin-left': this.contextSidebar?'0':`-${this.getContextWidth()}px`, width: `${this.getContextWidth()}px`  }
  }   
}
