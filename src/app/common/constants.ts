export const Constants = {
  /* views */
  VIEW_GLOBAL: 0,
  VIEW_CLUSTER: 1,
  VIEW_ARTICLE: 2,
  /* tabs */
  TAB_OVERVIEW: 0,
  TAB_CONSUMPTION: 1,
  TAB_CHATTER: 2,
  TAB_ENGAGEMENT: 3,

  /* time filter */
  PERIOD_LIST: ['custom', '1 day', '7 days', '1 months', '6 months'],

  /* search part */
  SEARCH_PART: ['all','clusters','articles'],

  /* chart color */
  CHART_COLORS: ["#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1"],
  /* clusters sort */
  CLASTERS_SORT_LIST: [
    'Brandto_Commendts +', 
    'Prrt facebook_likes +', 
    'Prrt facebook_shares +', 
    'CT Total Engagement +', 
    'Unique URLs'
  ],

  /* display number of items */
  CLASTERS_NUM_LIST: [
    10, 15, 20, 25
  ],

  /* default dashboard */
  DEFAULT_DASHBOARD: {
    id: null,
    title: 'New dashboard', 
    search: {term: '', part: '0'}, 
    period: {index:1, startDate: null, endDate: null},
    clusters: {items: 10, sort: [], page: 1},
    platforms: {items: 10, page: 1},
    articles: {page: 1}
  },


  /* urls for API */
  URL: {
    baseURL: 'http://mybackend.com/api'
  }
}




