import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { DataTable } from '../../../../../node_modules/angular-6-datatable';
import { AppService } from '../../../app.service';
import { Constants } from '../../../common/constants'
@Component({
  selector: 'app-global-consumption',
  templateUrl: './global-consumption.component.html',
  styleUrls: ['./global-consumption.component.scss']
})
export class GlobalConsumptionComponent implements OnInit {

  /* data table */
  data: any; 
  currentPage: number;
  totalPages: number;
  totalRows: number;
  rowsOnPage: number;
  
  currentArticle: any;

  /* global filters */
  filter: any;

  @ViewChild('mf') childDataTable:DataTable;


  constructor(public appService: AppService) { 

		this.appService.onCurrentSectionChanged.subscribe(section => {
			this.getConsumptionData();
    });
    
		this.appService.onCurrentDashboardChanged.subscribe(dashboard => {
			this.getConsumptionData();
		});

  }

  getConsumptionData() {
		if (this.appService.currentSection.articles && this.appService.currentSection.articles.length) {
			for (let i = 0; i < this.data.length; i++) {
				if (this.data[i].id === this.appService.currentSection.articles[0]) {
					this.currentArticle = this.data[i];
					break;
				}
			}
		} else {
			this.currentArticle = false; // global view 
			let today = new Date();
			let ymd = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;

			let currentFilter = {
				clusters: this.appService.currentSection.clusters,
				platforms: this.appService.currentSection.platforms,
				startDate: this.appService.currentDashboard.period.startDate,
				endDate: this.appService.currentDashboard.period.endDate
			}
			/* if not setted dates, use today */
			if (!currentFilter.startDate) {
				currentFilter.startDate = ymd;
				currentFilter.endDate = ymd;
			}
			if (JSON.stringify(currentFilter) != JSON.stringify(this.filter)) {
				this.appService.getConsumption(currentFilter).subscribe(data => {
					this.data = data;
					this.filter = { ...currentFilter
					};
					this.totalRows = this.data.length;
					this.totalPages = Math.ceil(this.totalRows / this.rowsOnPage);
					this.currentPage = 1
				})
			}

		}
	}

  ngOnInit() {
    this.rowsOnPage = 5;
  }

  setPage (pageNumber) {
    this.currentPage = pageNumber
    this.childDataTable.setPage(this.currentPage, this.rowsOnPage)
  }

  hideAllMenu () {
    let menuList =  Array.from(document.querySelectorAll('div.menu-wrapper') as HTMLCollectionOf<HTMLElement>);
    menuList.forEach(function(element) {
      element.style.display = 'none';
    });
  }
}
