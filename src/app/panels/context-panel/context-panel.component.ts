import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { AppService } from '../../app.service';
import { Constants } from '../../common/constants'
import { random } from '../../../../node_modules/@types/lodash-es';
// import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-context-panel',
  templateUrl: './context-panel.component.html',
  styleUrls: ['./context-panel.component.scss']  
})
export class ContextPanelComponent implements OnInit {
  
  /* global service variable */
  isSaved: boolean;
  currentDashboard: any;

  /* date picker model */
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
  }
  
  public dateStartModel: any; // = { date: { year: 2018, month: 0, day: 9 } };
  public dateEndModel: any; // = { date: { year: 2018, month: 10, day: 9 } };


  /* ui part */
  showPeriodMenu: boolean = false;
  searchPart: String[] = Constants.SEARCH_PART;
  periodList: String[] = Constants.PERIOD_LIST;
  clusterSortList: String[] = Constants.CLASTERS_SORT_LIST;
  clusterNumList: number[] = Constants.CLASTERS_NUM_LIST;


  currentSearchIndex: number = 0;
  showSearchMenu: boolean = false;
  showClusterNumMenu: boolean = false;
  showClusterSortMenu: boolean = false;
  showPlatformNumMenu: boolean = false;
  showArticlePageMenu: boolean = false;
  




  @Input() contextWidth: number = 500;

  /* update signal */
  @Output() onUpdateEntry: EventEmitter<object> = new EventEmitter<object>();
  @Output() onClickHide: EventEmitter<object> = new EventEmitter<object>();

  /* main expression */
  clusterData: any;
  platformData: any;
  articleData: any;
  articlePageNumber: number = 1;
  articleItemsPerPage: number = 5;
  articleTotalPages: number = 0;

  constructor(public appService: AppService) {
    this.appService.onCurrentDashboardChanged.subscribe(dashboard => {        
      this.currentDashboard = dashboard;
      this.setDateModel();
    });
    this.appService.onIsSavedChanged.subscribe(saved => {        
      this.isSaved = saved;
      // this.setDateModel();
    });
  }
  

  ngOnInit() {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
    let yyyy = today.getFullYear();
    this.dateStartModel = { date: { year: yyyy, month: mm, day: dd } };
    this.dateEndModel = { date: { year: yyyy, month: mm, day: dd } };
  }

  getModelFromDate(date) {
    const dd = date.getDate();
    const mm = date.getMonth() + 1; //January is 0!
    const yyyy = date.getFullYear();
    return { date: { year: yyyy, month: mm, day: dd } };
  }
  getModelFromeDateString (dt) {
    let ymd = dt.split('-');
    return { date: { year: parseInt(ymd[0]), month: parseInt(ymd[1]), day: parseInt(ymd[2]) } };
  }

  getDateStringFromModel (dateModel) {
    return `${dateModel.date.year}-${dateModel.date.month}-${dateModel.date.day}`;
  }

  setPeriodIndex(index) {
    this.currentDashboard.period.index = index; 
    this.showPeriodMenu = !this.showPeriodMenu;
    this.currentDashboard.period.startDate = this.getDateStringFromModel(this.dateStartModel)
    this.currentDashboard.period.endDate = this.getDateStringFromModel(this.dateEndModel)
    this.setDateModel();
  }

  setDateModel() {
    const today = new Date();
    
    let oneWeekAgo = new Date();
    oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);

    let oneMonthAgo = new Date();
    oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1);

    let sixMonthsAgo = new Date();
    sixMonthsAgo.setMonth(sixMonthsAgo.getMonth() - 6);


    switch ( this.currentDashboard.period.index ) {
      case 0: // custom
        this.dateStartModel = this.getModelFromeDateString (this.currentDashboard.period.startDate);
        this.dateEndModel = this.getModelFromeDateString (this.currentDashboard.period.endDate);
        break;
      case 1: // 1 day
        this.dateStartModel = this.getModelFromDate (today);
        this.dateEndModel = this.getModelFromDate (today);
        break;
      case 2: // 7 days
        this.dateStartModel = this.getModelFromDate (oneWeekAgo);
        this.dateEndModel = this.getModelFromDate (today);
        break;
      case 3: // 1 months
        this.dateStartModel = this.getModelFromDate (oneMonthAgo);
        this.dateEndModel = this.getModelFromDate (today);
        break;
      case 4: // 6 months
        this.dateStartModel = this.getModelFromDate (sixMonthsAgo);
        this.dateEndModel = this.getModelFromDate (today);
        break;
    }

    this.updateContent();
  }

  onDateChanged (event) {
    this.updateContent();
  }

  updateContent () {
    this.updateClusters ();
    this.updatePlatforms ();
    this.updateArticles ();
    this.appService.setSaveState(false);
    this.appService.setCurrentSectionChanged({});
  }

  updateClusters () {
    const condition = {
      numberOfClusters: this.currentDashboard.clusters.items,
      startDate: this.currentDashboard.period.startDate,
      endDate: this.currentDashboard.period.endDate,
      sortBy: this.currentDashboard.clusters.sort
    }
    this.appService.getClusters(condition).subscribe(data => {
      /* should be removed after backend */
     
      let resData = [];
      resData.push(data);
      for (let i = 0; i < resData[0].length; i++) {
        resData[0][i].radius = Math.random() * 100;
      }
      
      this.clusterData = resData[0];
    });
  }

  updatePlatforms () {
    const condition = {
      numberOfPlatforms: this.currentDashboard.platforms.items,
      startDate: this.currentDashboard.period.startDate,
      endDate: this.currentDashboard.period.endDate,
      clusterUris: null
    }
    this.appService.getPlatforms(condition).subscribe(data => {
      let tot = 0;
      let resData = [];
      resData.push(data);

      for (let i = 0; i < resData[0].length; i ++) {
        /* should be removed after backend */
        resData[0][i].val = Math.random() * 10;
        tot+=resData[0][i].val;
      }
      
      this.platformData = [
        {
          type: 'platforms',
          unit: '',
          total: tot,
          data: resData[0]
        }
      ]
    });
  }

  updateArticles () {
    const condition = {
      numberOfClusters: this.currentDashboard.clusters.items,
      startDate: this.currentDashboard.period.startDate,
      endDate: this.currentDashboard.period.endDate,
      clusterUris: null
    }
    this.appService.getArticles(condition).subscribe(data => {
      this.articlePageNumber = 1;
      this.articleData = data;
      this.articleTotalPages = Math.ceil(this.articleData.length / this.articleItemsPerPage)
    });
  }

  bufToArray (buffer) {
    let array = new Array();
    for (let data of buffer.values()) array.push(data);
    return array;
  }
  onChangedTitle(event) {
    this.currentDashboard.title = event.srcElement.value;
    this.appService.setSaveState(false);
  }

  saveEntry() {
    if (this.currentDashboard.id) { // update
      this.appService.updateEntry(this.currentDashboard).subscribe(data => {
        this.onUpdateEntry.emit(this.currentDashboard);
      });
    } else { // new (create)
      this.currentDashboard.id = Math.random()*1000 // temp (should be removed after backend)
      this.appService.createEntry(this.currentDashboard).subscribe(data => {
        this.onUpdateEntry.emit(this.currentDashboard);
      });
    }
    this.appService.setSaveState(true);

  }

  onSelectCluster(e) {
    const section = {
      clusters: [ e.id ],
      platforms: [],
      articles: []
    }
    this.appService.setCurrentSectionChanged(section);
  }
  onSelectPlatforms(e) {
    let platforms = [];
    for (let i = 0; i < e.length; i ++ ) {
      platforms.push (e[i].id);
    }
    const section = {
      clusters: this.appService.currentSection.clusters,
      platforms: platforms,
      articles: []
    }
    this.appService.setCurrentSectionChanged(section);
  }

  getSumFromArray(obj) {
    let sum = 0;
    for (let i = 0; i < obj.length; i ++) {
      sum += obj[i];
    }
    return sum;
  }

  onSelectArticle(eid) {
    let section = { ... this.appService.currentSection };
    section ['articles'] = [eid];
    this.appService.setCurrentSectionChanged(section);
  }


  hideSidbar() {
    this.onClickHide.emit();
  }

  hideAllMenu () {
    let menuList =  Array.from(document.querySelectorAll('div.menu-wrapper') as HTMLCollectionOf<HTMLElement>);
    menuList.forEach(function(element) {
      element.style.display = 'none';
    });
  }

}
